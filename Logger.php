<?php

namespace Inovva\Logger;


use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;
use Psr\Log\LogLevel;

class Logger extends AbstractLogger implements LoggerInterface {

    use LoggerTrait;

    protected static $instance;
    protected static $dir = '';
    protected static $filename = "log.txt";
    protected $log_local = LOG_LOCAL0;
    protected $log_level = LogLevel::NOTICE;
    private $uniqueID;

    /**
     * @param int $level
     * @return Logger
     */
    public static function getInstance($level = LogLevel::NOTICE) {

        if (null === static::$instance) {
            static::$instance = new Logger($level, self::$dir);
        }

        return self::$instance;
    }

    protected function __construct($level, $dir) {
        $this->setLevel($level);
        $dir = ($dir == '') ? getcwd() . '/log/' : $dir;

        $this->setDir($dir);
        $this->uniqueID = uniqid();
    }

    public function log($level, $message, array $context = array())
    {
        if ($level <= $this->log_level){
            $msg = $this->getMsgWithCaller($this->interpolate($message, $context));
            return $this->_write($msg, $level);
        }
        return false;
    }


    public function setLevel($level) {
        if (is_numeric($level)) {
            $this->log_level = $level + 0;
        }
        return $this->log_level;
    }

    public function setFilename($filename) {
        if (!empty($filename)) {
            static::$filename = preg_replace('/[^a-z0-9-\_\.]/i', "_", $filename);
            return true;
        } else {
            static::$filename = "log.txt";
        }
        return static::$filename;
    }

    public function setDir($dir) {
        if (!empty($dir)) {
            static::$dir = preg_replace('/[^a-z0-9-\_\.\\\\\\/:\~]/i', "", $dir);
        } else {
            if (defined('ABSPATH')){
                static::$dir = ABSPATH;
            } else {
                static::$dir = __DIR__;
            }
        }

        return static::$dir;
    }

    /**
     * @param int $log_local
     */
    public function setLogLocal($log_local)
    {
        $this->log_local = $log_local;
    }

    public function getFilePath() {
        return $this->filepath();
    }

    private function filepath() {

        $filePath = static::$dir . (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? '\\' : '/') . static::$filename;
        return $filePath;
    }

    private function _write($msg, $level) {
        $theTime = gettimeofday();
        $text = "[" . date('Y-m-d\TH:i:s') . '.' . str_pad($theTime['usec'], 6, '0', STR_PAD_LEFT) . "] [" . $this->uniqueID  . "] " . $level . " : " . $msg . "\r\n";
        file_put_contents($this->filepath(),
            $text,
            FILE_APPEND);
        return $text;
    }

    private function getMsgWithCaller($msg) {
        $backtrace = debug_backtrace();
        // 3 for nesting [debug, info, warn, error, fatal]() > log() > getMsgWithCaller()
        if (count($backtrace) > 3) {
            $caller = "{$backtrace[3]['function']}:{$backtrace[2]['line']}";
            if (isset($backtrace[3]['class']))
                $caller = "{$backtrace[3]['class']}#" . $caller;
            return "in {$caller} - {$msg}";
        }
        return $msg;
    }

    private function interpolate($message, array $context = array())
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            $replace['{' . $key . '}'] = $val;
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }
}
